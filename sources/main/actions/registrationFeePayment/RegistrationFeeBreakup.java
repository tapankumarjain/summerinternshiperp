package actions.registrationFeePayment;

import java.io.IOException;
import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class RegistrationFeeBreakup
 */
@WebServlet("/RegistrationFeeBreakup")
public class RegistrationFeeBreakup extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RegistrationFeeBreakup() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.sendError(500);
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		
		int round = Integer.parseInt(request.getParameter("round"));
		int year = Integer.parseInt(request.getParameter("year"));
		String breakupGen = request.getParameter("fee_breakup_general");
		String breakupSc = request.getParameter("fee_breakup_sc");
		breakupGen = URLDecoder.decode(breakupGen, "UTF-8");
		breakupSc = URLDecoder.decode(breakupSc, "UTF-8");

		System.out.println(round + "\n" + year + "\n" + breakupGen + "\n" + breakupSc);
		postgreSQLDatabase.feePayment.RegistrationFeePaymentQuery.addRegistrationFeeBreakup(round, "GENERAL",
				breakupGen, year);
		postgreSQLDatabase.feePayment.RegistrationFeePaymentQuery.addRegistrationFeeBreakup(round, "SC", breakupSc,
				year);
		postgreSQLDatabase.feePayment.RegistrationFeePaymentQuery.addRegistrationFeeBreakup(round, "ST", breakupSc,
				year);
		postgreSQLDatabase.feePayment.RegistrationFeePaymentQuery.addRegistrationFeeBreakup(round, "OBC", breakupGen,
				year);

	}

}
