package actions.ftp;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.itextpdf.text.List;
import com.itextpdf.text.pdf.hyphenation.TernaryTree.Iterator;

import ftp.FTPUpload;

/**
 * Servlet implementation class FtpUpload
 */
@WebServlet("/document-upload")
@MultipartConfig(fileSizeThreshold=1024*1024*2, // 2MB
                 maxFileSize=1024*1024*10,      // 10MB
                 maxRequestSize=1024*1024*50)  
public class FtpUpload extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	
	 
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FtpUpload() {
        super();
        
        
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter writer=response.getWriter();
	System.out.println(request.getParameter("directory"));
	writer.write(request.getParameter("directory"));
    for (Part part : request.getParts()) {
    
       String fileName = utilities.ServletUtils.getFileName(part);
      
        FTPUpload.uploadFile(part.getInputStream(),request.getParameter("directory")+fileName);
        
  
     }
     }
        
        
       
    }
    
	
	


