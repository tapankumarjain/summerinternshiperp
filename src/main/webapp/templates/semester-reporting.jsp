<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="postgreSQLDatabase.registration.Query"%>
    <%@page import="users.Student"%>
    <%@page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" >
<style type="text/css">
@page{
size:A4;
}
table, tr, td, th {border:1px solid black; border-collapse:collapse;}
</style>
<title>IIITK | ERP</title>
</head>
<body>
<header align="center" ><h2>Student Registration ID List</h2></header>
<div class="box-body" style="font-size:80%" align="center">
								<table id="example1" class="table table-bordered table-striped" >
									<thead style="font-size:120%">
										<tr>
											<th>S.N.</th>
											<th>Name</th>
											<th>Student ID</th>						
											<th>Registration ID</th>
											
											</tr>
									</thead>
									<tbody>
										<%
                	ArrayList<Student> csab_list=postgreSQLDatabase.registration.Query.getStudentRegistrationList();
                                Iterator<Student> iterator=csab_list.iterator();
                                int index=1;
                                while(iterator.hasNext()){
                    				Student current=iterator.next();
                    				if(current.getSemester()==1)continue;
                    				if(request.getParameter("reported")!=null&&request.getParameter("reported").equals("true") &&!current.isReported())continue;
                    				if(request.getParameter("reported")!=null&&request.getParameter("reported").equals("false") &&current.isReported())continue;
                    					
                %>
										<tr>
											<td><center><%=index++ %></center></td>
											<td><center><%=current.getName() %></center></td>
											<td><center><%=current.getStudent_id()%></center></td>
											<td><center><%=current.getRegistration_id()%></center></td>
											
											
										</tr>
										<%
                }
				%>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->

<script type="text/javascript">
window.print();
</script>
</body>
</html>